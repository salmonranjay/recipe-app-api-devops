# Instructions for how to configure terraform

terraform {
  backend "s3" {
    bucket                  = "recipe-app-api-ops-tfstate"
    key                     = "recipe-app.tfstate"
    region                  = "us-east-1"
    encrypt                 = true
    dynamodb_table          = "recipe-app-api-devops-tf-state-lock"
    profile                 = "aws_whitspace"
    shared_credentials_file = "~/.aws/credentials"
  }
}

provider "aws" {
  profile                 = "aws_whitspace"
  region                  = "us-east-1"
  version                 = "~> 2.54.0"
  shared_credentials_file = "~/.aws/credentials"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

